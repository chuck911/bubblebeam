const {getSubDomains, push, generateFromPattern, generateUrls, updateSites, sites, store, storeJobsId, addPushTask, refreshSites} = require('./libs')
const Router = require('koa-router')
const koaNunjucks = require('koa-nunjucks-2')
const path = require('path')
const koa = require('koa')
const iconv = require('iconv-lite')
const chardet = require('chardet')
const Day = require('dayjs')
const fs = require('fs-extra')
const Queue = require('bull')
const build = require('./builder')

const pushQueue = new Queue('push-queue-test')
const agreeQueue = new Queue('agree-queue-test')
const taskQueue = new Queue('task-queue-test')
const koaBody = require("koa-body")
const session = require('koa-session')
const app = new koa()
const router = new Router()

const sitesJson = './data/sites.json'

app.use(koaNunjucks({
	ext: 'njk',
	path: path.join(__dirname, 'views')
}))

app.use(session({ maxAge: 86400000*30 }, app))
	.use(async (ctx, next) => {
		if (ctx.path !== '/login' && !ctx.session.user) {
			ctx.redirect('/login')
		}
		await next()
	})
app.keys = ['wHosy0urDAddY?']

router.get('/login', async ctx => {
	await ctx.render('login')
})

router.post('/login', async ctx => {
	const postData = ctx.request.body
	console.log(postData)
	if (postData.username == '4dm1n' && postData.password == 'pUsh4bL9') {
		ctx.session.user = 'admin'
		ctx.redirect('/list')
	} else {
		ctx.redirect('/login')
	}
})

router.get('/', async ctx => {
	ctx.redirect('/list')
})

router.get('/refresh', async ctx => {
	refreshSites()
	ctx.redirect('/list')
})

router.get('/logout', async ctx => {
	ctx.session = null
	ctx.redirect('/login')
})

router.get('/list', async (ctx, next)=>{
	const sites = fs.readJsonSync(sitesJson)
	await ctx.render('list', {sites, title:'网站列表'})
})

router.get('/site/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	const site = sites[domain]
	if (!site) return
	const subdomainFile = `./data/subdomains/${domain}.json`
	let subdomains = []
	if (fs.existsSync(subdomainFile)) {
		subdomains = await fs.readJson(subdomainFile)
	}
	// ctx.body = site
	await ctx.render('site', {site, subdomains, title:'网站管理 '+domain})
})

router.get('/edit/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	const site = sites[domain]
	if (!site) return
	await ctx.render('edit', {site, title:'修改网站 '+domain})
})

router.post('/edit/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	const site = ctx.request.body
	updateSites(sites => {
		sites[site.domain] = site
		return sites
	})
	ctx.redirect('/site/'+domain)
})

router.get('/addsite', async (ctx, next)=>{
	await ctx.render('addsite', {title:'添加网站'})
})

router.post('/addsite', async (ctx, next)=>{
	const site = ctx.request.body
	updateSites(sites => {
		sites[site.domain] = site
		return sites
	})
	ctx.body = sites
	ctx.redirect('/site/'+site.domain)
	// await ctx.render('addsite', {})
})

router.get('/import', async (ctx, next)=>{
	await ctx.render('import', {title: '导入配置文件'})
})

router.post('/import', async (ctx, next)=>{
	const file = ctx.request.files.config
	let content = fs.readFileSync(file.path)
	const encoding = chardet.detect(content)
	content = iconv.decode(content, encoding)
	let confs
	try {
		confs = JSON.parse(content)
		for (const domain in confs) {
			confs[domain]['domain'] = domain
			confs[domain]['urlPattern'] = confs[domain]['推送地址生成规则']
			delete confs[domain]['推送地址生成规则']
			delete confs[domain]['子站域名生成规则']
		}
	} catch (error) {
		return ctx.body = '不是合法的json文件'
	}
	updateSites(sites => {
		Object.assign(sites, confs)
	})
	
	ctx.redirect('/list')
})

router.get('/syncSubdomains/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	const size = ctx.query.size || 1000
	try {
		const subdomains = await getSubDomains(domain, size)
		fs.writeFileSync(`./data/subdomains/${domain}.json`, JSON.stringify(subdomains.list, null, "\t"))
		ctx.redirect('/site/'+domain)
	} catch (error) {
		return ctx.body = error+'<p>请求不成功，请稍后再试</p>'
	}
})

router.post('/addPushTask/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	const subdomains = await fs.readJson(`./data/subdomains/${domain}.json`)
	const jobIds = []
	for (const subdomain of subdomains) {
		const job = await addPushTask(pushQueue, domain ,subdomain, 100)
		jobIds.push(job.id)
		// break
	}

	const date = Day().format('YYYY-MM-DD')
	await storeJobsId(domain, date, jobIds)

	ctx.type = 'html'
	ctx.body = `任务添加成功！ <a href="/site/${domain}">返回</a>`
})

router.post('/agreeSubdomains/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	agreeQueue.add({name:`batch mip agree ${domain}`, type:'batch', domain})
	ctx.type = 'html'
	ctx.body = `正在批量同意协议，稍后将自动完成。 <a href="/site/${domain}">返回</a>`
})

router.get('/tasks/:domain', async (ctx, next)=>{
	const domain = ctx.params.domain
	const site = sites[domain]
	const dir = `./data/jobs/${domain}/`

	if (!fs.existsSync(dir)) {
		await ctx.render('sitetasks', {lastestJobs:[], site})
		return
	}
	const files = Array.from(await fs.readdir(dir)).sort((a,b)=>a<b?1:-1)

	const currentFile = files.shift()
	
	const currentDate = currentFile.replace('.json', '')
	const dates = files.map(filename=> filename.replace('.json', ''))
	let lastestJobIds = await fs.readJson(dir+currentFile)
	lastestJobIds = lastestJobIds.sort((a,b)=> b-a).slice(0,100)

	const lastestJobs = await Promise.all(lastestJobIds.map(
		id => pushQueue.getJob(id).then(job=>{
			return job.getState().then(state =>Object.assign(job.toJSON(), {state}))
		})
	))
	// const jobDomains = .map(job => job.data.name)
	
	await ctx.render('sitetasks', {lastestJobs, site, currentDate, dates, title: domain+'任务列表'})
})

router.get('/tasks/:domain/:date', async (ctx, next)=>{
	const domain = ctx.params.domain
	const currentDate = ctx.params.date
	const site = sites[domain]
	const dir = `./data/jobs/${domain}/`
	const currentFile = dir+currentDate+'.json'

	if (!fs.existsSync(currentFile)) {
		await ctx.render('sitetasks', {lastestJobs:[], site})
		return
	}
	const files = Array.from(await fs.readdir(dir)).sort((a,b)=>a<b?1:-1)
		.filter(filename => filename!=currentDate+'.json')
	
	const dates = files.map(filename=> filename.replace('.json', ''))
	const lastestJobIds = await fs.readJson(currentFile)

	const lastestJobs = await Promise.all(lastestJobIds.map(
		id => pushQueue.getJob(id).then(job=>{
			return job.getState().then(state =>Object.assign(job.toJSON(), {state}))
		})
	))
	const title = `${domain} ${currentDate}任务列表`
	await ctx.render('sitetasks', {lastestJobs, site, currentDate, dates, title})
})

router.get('/importSubdomain/:domain', async (ctx) => {
	const domain = ctx.params.domain
	await ctx.render('importSubdomain', {domain, title: '导入子域名文件'})
})

router.post('/importSubdomain/:domain', async (ctx) => {
	const domain = ctx.params.domain
	const file = ctx.request.files.subdomains
	let content = fs.readFileSync(file.path, {encoding: 'utf-8'})
	const subdomains = content.split(/\r?\n/g).filter(s=>s&&s.trim())
	fs.writeFileSync(`./data/subdomains/${domain}.json`, JSON.stringify(subdomains, null, "\t"))
	ctx.redirect('/site/'+domain)
})

async function tasksHandler(ctx, next) {
	const state = ctx.params.state || 'completed'
	const labels = {completed:'已经完成', failed:'失败', waiting:'等待', delayed:'推迟'}
	const jobCounts = await pushQueue.getJobCounts()
	const jobs = await pushQueue.getJobs([state], 0, 100, false)
	await ctx.render('tasks', {jobCounts, labels, state, jobs, title:'所有推送任务'})
}

router.get('/tasks', tasksHandler)
router.get('/tasks-:state', tasksHandler)

router.get('/task/:id', async (ctx, next) => {
	const id = ctx.params.id
	const job = await pushQueue.getJob(id)
	const state = await job.getState()
	const date = Day(job.timestamp).format('YYYY-MM-DD HH:mm:ss')
	await ctx.render('task', {job, state, date, title:'推送任务详情'+id})
})

router.get('/schedule', async ctx =>{
	let jobs = await taskQueue.getRepeatableJobs()
	jobs = jobs.map(job => {
		const cron = job.cron.split(' ')
		const hour = cron[1].padStart(2, "0")
		const minute = cron[0].padStart(2, "0")
		const next = Day(job.next).format('YYYY-MM-DD HH:mm:ss')
		return {time: `${hour}:${minute}:00`, next}
	})
	await ctx.render('schedule', {jobs, title:'定时任务'})
})

router.post('/delete/:domain', async (ctx) => {
	const domain = ctx.params.domain
	updateSites(sites => {
		delete sites[domain]
		return sites
	})
	ctx.redirect('/list')
})

router.get('/build', async ctx => {
	await ctx.render('build', {title:'站点生成'})
})

router.post('/build', async ctx => {
	const txt = ctx.request.body.domains
	const domains = txt.split(/\r?\n/g).filter(s=>s).map(d => d.trim())
	try {
		for (const domain of domains) {
			await build(domain)
		}
		ctx.type = 'html'
		ctx.body = `已生成${domains.length}个站点，重载nginx后即可生效。 <a href="/build">继续添加</a>`
	} catch (error) {
		ctx.body = error
	}
})

router.get('/enable/:domain', async ctx => {
	const domain = ctx.params.domain
	updateSites(sites => {
		delete sites[domain].invalid
		return sites
	})
	ctx.redirect('/site/'+domain)
})

router.post('/disable/:domain', async (ctx) => {
	const domain = ctx.params.domain
	updateSites(sites => {
		sites[domain].invalid = true
		return sites
	})
	ctx.redirect('/site/'+domain)
})

app.use(koaBody({ multipart: true }))
	.use(router.routes())
	.use(router.allowedMethods())

app.listen(3028)