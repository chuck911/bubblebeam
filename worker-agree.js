const Queue = require('bull')
const fs = require('fs-extra')

const {push, addPushTask, storeJobsId, agreeMIP} = require('./libs')
const delay = (time) => (result) => new Promise(resolve => setTimeout(() => resolve(result), time));
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const agreeQueue = new Queue('agree-queue-test')

let count = 0
agreeQueue.process(async (job)=>{
	const {type, domain} = job.data
	if (type=='batch') {
		const subdomains = await fs.readJson(`./data/subdomains/${domain}.json`)
		for (const subdomain of subdomains) {
			await agreeQueue.add({name:`mip agree ${domain}`, type:'agree', domain: subdomain}, {attempts: 3, backoff: 10000})
		}
	} else {
		console.log(type)
		console.time('agreeMIP');
        await sleep(6000)
        if (count==10) {
            count = 0
            await sleep(100000)
        }
        const res = await agreeMIP(domain)
        console.log('sleep?')
        console.timeEnd('agreeMIP');
        count++
		return res.body
	}
})