const Queue = require('bull')
const cleanQueue = new Queue('clean-queue')
const pushQueue = new Queue('push-queue-test')


cleanQueue.process((job, done)=>{
	pushQueue.clean(1000*60*60*10, 'completed')
	pushQueue.on('cleaned', function(jobs, type) {
		done(null, jobs.length)
	})
})