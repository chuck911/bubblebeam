const Queue = require('bull')
const fs = require('fs-extra')
const Day = require('dayjs')
const {push, addPushTask, storeJobsId, agreeMIP, refreshSites, updateSites} = require('./libs')

const pushQueue = new Queue('push-queue-test')
const maxSize = 100

const errors = {}

pushQueue.process(10, async (job)=>{
	// console.log(job.data)
	const {subdomain, domain, urls} = job.data
	refreshSites()
	// console.log(subdomain)
	try {
		const res = await push(subdomain, urls)
		const result = JSON.parse(res.body)
		if (result.remain_mip>0) {
			const size = Math.min(result.remain_mip, maxSize)
			const job = await addPushTask(pushQueue, domain ,subdomain, size)
			const date = Day().format('YYYY-MM-DD')
			await storeJobsId(domain, date, [job.id])
		}
		if (result.success<1) {
			// throw new Error('未推送成功，可能配额超限: '+JSON.stringify(result))
			return result
		}
		
		return result
	} catch (error) {
		if (error.statusCode=='401') {
			if (!errors[domain]) errors[domain] = 0
			errors[domain]++
			if (errors[domain]>50) {
				updateSites(sites => {
					sites[domain].invalid = true
					return sites
				})
			}
			return error.body
		} else {
			throw error
		}
	}
	
})
