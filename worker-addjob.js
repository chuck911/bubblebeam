const Queue = require('bull')
const fs = require('fs-extra')
const Day = require('dayjs')
const samples = require('lodash.samplesize')
const {push, addPushTask, storeJobsId, agreeMIP, refreshSites} = require('./libs')

const pushQueue = new Queue('push-queue-test')
const taskQueue = new Queue('task-queue-test')

const date = Day().format('YYYY-MM-DD')
const max = 1000

taskQueue.process(async (_job)=>{
    const subdomainClear = {}
    refreshSites()
	const sites = await fs.readJson('./data/sites.json')
    
    for (let i = 0; i < max; i++) {
        
        for (const domain in sites) {
            const site = sites[domain]
            if (site.invalid) continue
            if (subdomainClear[domain]) continue
            if (!fs.existsSync(`./data/subdomains/${domain}.json`)) continue
            
            let subdomains = await fs.readJson(`./data/subdomains/${domain}.json`)
            if (!subdomains[i]) {
                subdomainClear[domain] = true
            }

            const job = await addPushTask(pushQueue, domain ,subdomains[i], 100)
            //console.log(domain ,subdomains[i])
            await storeJobsId(domain, date, [job.id])
        }
    }
})
