const Queue = require('bull')
const cleanQueue = new Queue('clean-queue')

const repeatOpts = {
    cron: '40 23 * * *',
    tz: 'Asia/Shanghai'
}

;(async ()=>{
    const job = await cleanQueue.add({name:'clean jobs'},{repeat:repeatOpts})
    console.log(job)
})()