const arena = require('bull-arena')

arena({
    host: "127.0.0.1",
    queues: [
        {
            name: 'push-queue-test',
            "hostId": "local",
        },
        {
            name: 'task-queue-test',
            "hostId": "local",
        },
        {
            name: 'agree-queue-test',
            "hostId": "local",
        },
        {
            name: 'clean-queue',
            "hostId": "local",
        }
    ],
    // basePath: '/q',
})