const got = require('got')
const randomize = require('randomatic')
const fs = require('fs-extra')
const Day = require('dayjs')

const sitesJson = './data/sites.json'
let sites = require(sitesJson)

function refreshSites() {
	const _sites = fs.readJsonSync(sitesJson)
	Object.assign(sites, _sites)
}

function updateSites(update) {
	if (typeof update == "function") {
		Object.assign(sites, update(sites))
	} else {
		Object.assign(sites, update)
	}
    store()
}

function store() {
    fs.writeFileSync(sitesJson, JSON.stringify(sites, null, "\t"))
}

// const list = await getSubDomains('lsgushi.com')
async function getSubDomains(rootDomain, size=100, page=1) {
	const conf = sites[rootDomain]
	if (!conf) throw new Error('rootDomain '+rootDomain+' not found!')
	const res = await got.post('https://ziyuan.baidu.com/site/getsite?page='+page+'&pagesize='+size, {
		headers: {
			'Cookie': conf['cookie']
		},
		json: true
	})
	// console.log(res.body)
	const list = res.body.list
		.filter(item => item.root_domain==rootDomain && item.state==4)
		.map(item => item.url)
		.map(url => {
			if (url.startsWith('http')) {
				const _url = new URL(url)
				url = _url.host
				return url
			}
		})
	return {list, count: res.body.count}
}

// const res = await push('lianyun.lsgushi.com', [
// 	'https://lianyun.lsgushi.com/ruanzhuang/',
// 	'https://lianyun.lsgushi.com/ruanzhuang/80k3g32.html'
// ])
function getSiteConf(domain) {
	const rootDomain = domain.split('.').slice(1).join('.')
	if (sites[rootDomain]) {
		return sites[rootDomain]
	} else if (sites[domain]) {
		return sites[domain]
	} else {
		throw new Error('rootDomain '+rootDomain+' not found!')
	}
}

function push(domain, urls, force=false) {
	const conf = getSiteConf(domain)
	const token = conf.token
	if (!force && conf.invalid) return {body: '{"success": 0, "error": "site invalid"}'}

	const api = `http://data.zz.baidu.com/urls?site=${domain}&token=${token}&type=mip`
    return got.post(api, {
        body: urls.join('\n'),
        headers: {
			'Content-Type': 'text/plain',
			// 'Cookie': conf['cookie']
		}
    })
}

function agreeMIP(subdomain) {
	const site = getSiteConf(subdomain)
	return got.post(`https://ziyuan.baidu.com/mip/agreemip?site=http://${subdomain}/`, {
		headers: {
			'Cookie': site.cookie,
			'X-Request-By': 'baidu.ajax',
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		body: 'isAgreeProtool=1'
	})
}

// generateFromPattern('news-{数字}{字母-2}{数字-1}.aac49.cn')
function generateFromPattern(template) {
	const mapping = {'数字':'0', '字母': 'a'}
	const tagRegex = /\{([^\^\-}]+)\-?(\d+)?\}/g
	return template.replace(tagRegex, (match, tag, size)=>{
		if (!mapping[tag]) return '{'+tag+'}'
		return randomize(mapping[tag], size?size:3)
	})
}

//generateUrls('{域名}\/{字母}{字母}\/{数字}{数字}.html', 'lianyun.lsgushi.com', 10)
function generateUrls(template, domain, size) {
	template = template.replace('{域名}', 'http://'+domain)
	return [...Array(size)]
		.map(x => generateFromPattern(template))
}

async function storeJobsId(domain, date, jobIds) {
	const file = `./data/jobs/${domain}/${date}.json`
	let _jobIds = []
	if (fs.existsSync(file)) {
		_jobIds = await fs.readJson(file)
	}
	jobIds = _jobIds.concat(jobIds)
	await fs.outputJson(file, jobIds)
}

async function addPushTask(pushQueue, domain ,subdomain, size=100) {
	const conf = sites[domain]
	const urls = generateUrls(conf.urlPattern, subdomain, size)
	const job = await pushQueue.add({urls, domain, subdomain, name:subdomain}, {attempts: 2, backoff: 10000})
	return job
}

module.exports = {getSubDomains, push, generateFromPattern, generateUrls,
updateSites, sites, store, storeJobsId, addPushTask, agreeMIP, getSiteConf, refreshSites}