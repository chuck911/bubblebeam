const fs = require('fs-extra')
const nunjucks = require('nunjucks')
const util = require('util')
const {resolve} = require("path")
const chownr = require('chownr')

const source = '/www/wwwroot/template-site'
const targetDir = '/www/wwwroot/'
const confDir = '/www/server/panel/vhost/nginx/'

const chown = util.promisify(chownr)

async function build(domain) {
	await fs.copy(source, targetDir+domain)
	const _domain = ('.'+domain).replace(/\./g, '\\.')
	const conf = nunjucks.render(resolve(__dirname, './views/conf.njk'), {domain, _domain})
	await fs.writeFile(confDir+domain+'.conf', conf)
	await chown(targetDir+domain, 1000, 1000)
}

module.exports = build