const {push, generateUrls} = require('./libs')
const samples = require('lodash.samplesize')
const got = require('got')
const fs = require('fs-extra')

;(async ()=>{
	const sites = require('./data/sites.json')
	let = invalidSites = []

	for (const domain in sites) {
		if (!fs.existsSync(`./data/subdomains/${domain}.json`)) continue
		const site = sites[domain]
		const _subdomains = await fs.readJson(`./data/subdomains/${domain}.json`)
		const subdomains = samples(_subdomains, 5)
		let i = 0
		for (const subdomain of subdomains) {
			const urls = generateUrls(site.urlPattern, subdomain, 10)
			try {
				const res = await push(subdomain, urls)
			} catch (error) {
				if (error.statusCode=='401') {
					i++
				}
			}
			console.log(subdomain,i)
			if (i>=3) {
				invalidSites.push(domain)
				break
			}
		}
	}
	fs.writeFileSync('./invalidSites.json', JSON.stringify(invalidSites, null, "\t"))
	
})()